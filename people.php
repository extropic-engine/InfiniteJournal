<?
include 'header.php';
?>

<link rel="stylesheet" type="text/css" href="themes/<?echo $userinfo['game_name']?>/people.css" />

<div class='page'>

<?

# If we are adding a new person
if (isset($_POST['new_person'])) {
  mysql_query('INSERT INTO people (character_id) VALUES ('.$userinfo['current_character'].')');
  $result = mysql_query('SELECT LAST_INSERT_ID() as person_id');
  $row = mysql_fetch_array($result);
  die('<meta http-equiv=refresh content="0;URL=people.php?detail='.$row['person_id'].'">');
}

# If we are trying to update a person's details
if (isset($_POST['person_id'])) {
  $person_id = mysql_real_escape_string($_POST['person_id']);
  $person_name = mysql_real_escape_string($_POST['person_name']);
  $person_notes = mysql_real_escape_string($_POST['person_notes']);
  $person_profession = mysql_real_escape_string($_POST['person_profession']);
  $result = mysql_query('SELECT user_id FROM people JOIN characters USING (character_id) JOIN users USING (user_id) WHERE person_id="'.$person_id.'"');
  if (mysql_num_rows($result) == 1) {
    $row = mysql_fetch_array($result);
    if ($row['user_id'] != $userinfo['user_id']) {
      die('No person matching that ID found.');
    }
    mysql_query('UPDATE people SET name="'.$person_name.'", notes="'.$person_notes.'", profession_id="'.$person_profession.'" WHERE person_id="'.$person_id.'"');
  } else {
    die("No person matching that ID found.");
  }
}

# If we are viewing a specific person in detail
if (isset($_GET['detail'])) {
  $person_id = mysql_real_escape_string($_GET['detail']);
  $result = mysql_query('SELECT people.name AS name, notes, profession_id FROM people ' .
                        'JOIN characters USING (character_id) ' .
                        'WHERE character_id="'.$userinfo['current_character'].'" AND person_id="'.$person_id.'"');
  if (mysql_num_rows($result) == 0) {
    echo 'No matching person found.';
  } else {
    $person = mysql_fetch_array($result);
    $professions = mysql_query('SELECT profession_id, name FROM professions WHERE game_id='.$userinfo['current_game']);
    ?>
    <div class="person_details">
      <div class="person_details_back"><a href='people.php'>Back to all people</a></div>
      <form action='people.php?details=<?echo $person_id?>' method='post'>
        <input type='hidden' name='person_id' value='<?echo $person_id?>'>
        <select name='person_profession'>
          <option value="0">--</option>
          <?
          while ($profession = mysql_fetch_array($professions)) {
            if ($profession['profession_id'] == $person['profession_id']) {
              echo '<option selected value="'.$profession['profession_id'].'">'.$profession['name'].'</option>';
            } else {
              echo '<option value="'.$profession['profession_id'].'">'.$profession['name'].'</option>';
            }
          }
          ?>
        </select>
        <input type="text" name="person_name" value="<?echo $person['name']?>">
        <textarea name='person_notes'><?echo $person['notes']?></textarea><br>
        <input type='submit' value='Save'>
      </form>
    </div>
    <?
  }

# If we are viewing a list of all people
} else {

  $people_result = mysql_query('SELECT person_id, people.name AS name, professions.name AS profession FROM people JOIN professions USING (profession_id) JOIN characters USING (character_id) WHERE character_id="'.$userinfo['current_character'].'" ORDER BY person_id');

  if (mysql_num_rows($people_result) > 0) {

    echo '<div class="people_list">';
    echo '<br><div class="people_list_header">People</div>';

    while($person = mysql_fetch_array($people_result)) {
      if (!$person['name']) {
        $person['name'] = 'Unnamed';
      }
      echo '<a href="people.php?detail='.$person['person_id'].'"><div class="people_list_item">'.$person['profession'].' '.$person['name'].'</div></a>';
    }
  }

  ?>
    <form action='people.php' method="post">
      <input type="submit" name="new_person" value="New Person">
    </form>
  <?

 }
?>
</div>
