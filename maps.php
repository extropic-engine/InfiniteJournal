<?
include 'header.php'
?>

<link rel="stylesheet" type="text/css" href="themes/<?echo $userinfo['game_name']?>/maps.css" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="maps.js"></script>

<div class="page">
  <div class="canvas_container">
    <canvas id="map_canvas">
      Your browser does not support HTML5, sorry!
    </canvas>
  </div>
  <div class="color_picker">
    <br>
    <input type="submit" id="white" value=" ">
    <input type="submit" id="yellow" value=" ">
    <input type="submit" id="fuchsia" value=" ">
    <input type="submit" id="red" value=" ">
    <input type="submit" id="silver" value=" ">
    <input type="submit" id="gray" value=" ">
    <input type="submit" id="olive" value=" ">
    <input type="submit" id="purple" value=" ">
    <input type="submit" id="maroon" value=" ">
    <input type="submit" id="aqua" value=" ">
    <input type="submit" id="lime" value=" ">
    <input type="submit" id="teal" value=" ">
    <input type="submit" id="green" value=" ">
    <input type="submit" id="blue" value=" ">
    <input type="submit" id="navy" value=" ">
    <input type="submit" id="black" value=" ">
    <canvas id='brush_canvas' width=32 height=32></canvas>
  </div>
  <div class="tools_container">
    <br>
    <input type="submit" id='pencil' value="Pencil">
    <input type="submit" id='rect' value="Rectangle">
    <input type="submit" id='line' value="Line">
    <input type="submit" id='clear' value='Clear'>
    <input type="submit" id='save' value='Save'>
  </div>
</div>

