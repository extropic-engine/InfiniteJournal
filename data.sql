-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: infinityjournal.db
-- Generation Time: Apr 24, 2013 at 06:54 AM
-- Server version: 5.3.12-MariaDB
-- PHP Version: 5.3.10-nfsn2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `infinite_journal`
--

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE IF NOT EXISTS `characters` (
  `character_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`character_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`character_id`, `game_id`, `user_id`, `name`) VALUES
(1, 1, 1, 'Amasei'),
(2, 2, 1, 'Sharadon'),
(3, 1, 1, 'Renshu'),
(4, 1, 2, 'Bob');

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE IF NOT EXISTS `games` (
  `game_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_name` varchar(64) NOT NULL,
  PRIMARY KEY (`game_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`game_id`, `game_name`) VALUES
(1, 'EverQuest'),
(2, 'Wizardry 8'),
(3, 'Morrowind'),
(4, 'Myst'),
(5, 'Dwarf Fortress');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `character_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `character_id`, `name`, `description`) VALUES
(1, 3, 'acrylia slate', 'A proof of my Shar Vahl citizenship.'),
(2, 4, 'Bob''s Pants', 'They are magic???');

-- --------------------------------------------------------

--
-- Table structure for table `journals`
--

CREATE TABLE IF NOT EXISTS `journals` (
  `journal_id` int(11) NOT NULL AUTO_INCREMENT,
  `character_id` int(11) NOT NULL,
  `journal_name` varchar(64) NOT NULL,
  PRIMARY KEY (`journal_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `journals`
--

INSERT INTO `journals` (`journal_id`, `character_id`, `journal_name`) VALUES
(1, 1, 'Amasei''s Travels'),
(2, 3, 'Renshu''s Travels'),
(3, 0, 'There: A Bob''s Tale'),
(4, 4, 'There: A Bob''s Tale'),
(5, 4, 'Back Again: A Bob''s Tale Part 2: The Bobening');

-- --------------------------------------------------------

--
-- Table structure for table `journal_entries`
--

CREATE TABLE IF NOT EXISTS `journal_entries` (
  `entry_id` int(11) NOT NULL AUTO_INCREMENT,
  `journal_id` int(11) NOT NULL,
  `entry_title` varchar(64) NOT NULL,
  `entry_text` text NOT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `journal_entries`
--

INSERT INTO `journal_entries` (`entry_id`, `journal_id`, `entry_title`, `entry_text`) VALUES
(1, 1, 'First day', 'I saw a rabbit.'),
(2, 1, 'Second Day', 'Murdered 5 people.'),
(3, 2, 'Day 1 - Applying for citizenship', 'I have arrived in Shar Vahl, having followed the instructions of the Dar Khura guild summons I was given. I met with a Vah Shir with the title of Elder Spiritualist Grawleh in the royal palace. He told me that I would receive spiritual training from the Dar Khura and that I must become a citizen of Shar Vahl. \r\n\r\nHe gave me an application which I was to take to someone named Registrar Bindarah. I learned that her office is located across from the South Gate which leads to Shadeweaver Thicket.\r\n\r\nAfter bringing the application to Registrar Bindarah, she sent me to run some errands while she processed it. First, I must get an acrylia slate from someone named Mignah, and then I must register with a city tax collector.\r\n\r\nI spoke with a guard and learned that Mignah could be found above a tavern next to the North Gate, while the tax collector was located in the bank, which is in the southeast corner of the Merchant District.\r\n\r\nAfter recovering both the acrylia slate and the tax collector''s seal I returned to Bindarah. She told me that there was one final step in becoming a citizen: I must gain audience with the king and swear fealty to him.\r\n\r\nOnce I had done this, I returned to Bindarah and she gave me the acrylia slate which serves as proof of my citizenship. Should I ever lose it, I can return to her to be issued a new one.'),
(4, 2, 'Day 2 - Training in the Dar Khura', 'I returned to Elder Spiritualist Grawleh as a citizen of Shar Vahl. He gave me an initiate''s cloak and told me to present my acrylia slate to Spiritist Fehril to begin my training.\r\n\r\nSpiritist Fehril has requested that I bring him three hairless hides of the rockhopper young. He told me that I could find them in the craters surrounding the city. I decided to venture out via the North Gate, which led me to Hollowshade Moor. There I quickly found the enemies were too powerful and there were no rockhopper young in sight, so I decided to head back to the South Gate and see what could be found there.'),
(5, 4, 'Day 1', 'I saw a squirrel.'),
(6, 5, 'Day 10893', 'No sign of land.');

-- --------------------------------------------------------

--
-- Table structure for table `owned_games`
--

CREATE TABLE IF NOT EXISTS `owned_games` (
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `current_character` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`game_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owned_games`
--

INSERT INTO `owned_games` (`user_id`, `game_id`, `current_character`) VALUES
(1, 1, 3),
(1, 2, 3),
(2, 1, 4),
(1, 3, 0),
(1, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE IF NOT EXISTS `people` (
  `person_id` int(11) NOT NULL AUTO_INCREMENT,
  `character_id` int(11) NOT NULL,
  `profession_id` int(11) NOT NULL,
  `race_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`person_id`, `character_id`, `profession_id`, `race_id`, `name`, `notes`) VALUES
(1, 3, 2, 0, 'Grawleh', 'He was the first person I met on my journey. \r\n\r\nI gave him my Dar Khura Guild Summons. He told me that the time has come for me to register for citizenship of Shar Vahl. I will receive spiritual training from the Dar Khura. He gave me an application which I must take to Registrar Bindarah. He told me her office was located across from the South Gate, which is the bridge leading to Shadeweaver Thicket.'),
(2, 3, 1, 0, 'Fehril', 'I met him in the room where I began my journey.\r\n\r\nI spoke to him, accidentally interrupting his meditation. He asked how I might be of assistance, and I, not knowing the right questions to ask, took my leave.'),
(3, 3, 3, 0, 'Cholsa', 'I met her in the room where I began my journey.\r\n\r\nShe did not respond to my hails.'),
(4, 3, 0, 0, 'Jaima Seyel', 'I met her in the room where I began my journey.\r\n\r\nShe told me that she was in need of an assistant and asked me to show her my Dar Khura Apprentice Cloak.'),
(5, 3, 0, 0, 'Kanaad', 'I met him in the room where I began my journey.\r\n\r\nWhen I hailed him, he seemed as if he was too worn out to respond.'),
(6, 3, 3, 0, 'Gefna', 'I met him in a room to the north of the place where I began my journey.\r\n\r\nHe sold spells for those of the Beastmaster profession.'),
(7, 3, 0, 0, 'Registrar Bindarah', 'I gave her my application for citizenship. While I was waiting for her to process it, she told me to take a certificate to a tax collector and obtain his seal. Also, to have someone named Mignah create an Acrylia slate for me. I should bring both items back to her when I am done.\r\n\r\nA guard told me that I can find Mignah above the tavern next to the North Gate. He said that I could find Tax Collector Khugra in the bank, which is in the south-eastern end of the Merchants'' Quarter.'),
(8, 0, 0, 0, '', ''),
(9, 4, 0, 0, 'Bob', 'Bob is the best.'),
(10, 4, 0, 0, 'Edward', '');

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE IF NOT EXISTS `places` (
  `place_id` int(11) NOT NULL AUTO_INCREMENT,
  `character_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`place_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `professions`
--

CREATE TABLE IF NOT EXISTS `professions` (
  `profession_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`profession_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `professions`
--

INSERT INTO `professions` (`profession_id`, `game_id`, `name`) VALUES
(1, 1, 'Spiritist'),
(2, 1, 'Elder Spiritist'),
(3, 1, 'Scribe'),
(0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `quests`
--

CREATE TABLE IF NOT EXISTS `quests` (
  `quest_id` int(11) NOT NULL AUTO_INCREMENT,
  `character_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`quest_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `quests`
--

INSERT INTO `quests` (`quest_id`, `character_id`, `name`, `description`) VALUES
(1, 3, 'something', 'murderiffic'),
(2, 0, '', ''),
(3, 4, 'Fetch me a bucket', 'I don''t know how to fetch the bucket.');

-- --------------------------------------------------------

--
-- Table structure for table `races`
--

CREATE TABLE IF NOT EXISTS `races` (
  `race_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`race_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `races`
--

INSERT INTO `races` (`race_id`, `game_id`, `name`) VALUES
(0, 0, ''),
(1, 1, 'Barbarian'),
(2, 1, 'Dark Elf'),
(3, 1, 'Drakkin'),
(4, 1, 'Dwarf'),
(5, 1, 'Erudite'),
(6, 1, 'Froglok'),
(7, 1, 'Gnome'),
(8, 1, 'Half-Elf'),
(9, 1, 'Halfling'),
(10, 1, 'High Elf'),
(11, 1, 'Human'),
(12, 1, 'Iksar'),
(13, 1, 'Ogre'),
(14, 1, 'Troll'),
(15, 1, 'Vah Shir'),
(16, 1, 'Wood Elf');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `fullname` varchar(128) DEFAULT NULL,
  `current_game` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `fullname`, `current_game`) VALUES
(1, 'extropic_engine', 'eb1c4fdf7bb75551b43df3cfe5cdae2d', 'Joshua Stewart', 1),
(2, 'Guest', '084e0343a0486ff05530df6c705c8bb4', 'Guest', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
