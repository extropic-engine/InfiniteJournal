<?
include 'header.php';
?>

<link rel="stylesheet" type="text/css" href="themes/<?echo $userinfo['game_name']?>/journal.css" />

<div class='page'>

<?

# If we are creating a new journal
if (isset($_POST['new_journal_name'])) {
  mysql_query('INSERT INTO journals (journal_name, character_id) VALUES ("'.mysql_real_escape_string($_POST['new_journal_name']).'", '.$userinfo['current_character'].')');
}

# If we are adding a new journal entry
if (isset($_POST['new_entry'])) {
  $journal_id = mysql_real_escape_string($_POST['journal_id']);
  $result = mysql_query('SELECT * FROM journals JOIN characters USING (character_id) JOIN users USING (user_id) WHERE journal_id="'.$journal_id.'" AND user_id="'.$userinfo['user_id'].'"');
  if (mysql_num_rows($result) != 1) {
    die("Could not find a journal matching the given ID.");
  }
  mysql_query('INSERT INTO journal_entries(journal_id) VALUES("'.$journal_id.'")');
  $result = mysql_query('SELECT LAST_INSERT_ID() as entry_id');
  $row = mysql_fetch_array($result);
  die('<meta http-equiv=refresh content="0;URL=journal.php?entry='.$row['entry_id'].'">');
}

# If we are trying to update a journal entry
if (isset($_POST['entry_id'])) {
  $entry_id = mysql_real_escape_string($_POST['entry_id']);
  $entry_title = mysql_real_escape_string($_POST['entry_title']);
  $entry_text = mysql_real_escape_string($_POST['entry_text']);
  $result = mysql_query('SELECT user_id FROM journal_entries JOIN journals USING (journal_id) JOIN characters USING (character_id) JOIN users USING (user_id) WHERE entry_id="'.$entry_id.'"');
  if (mysql_num_rows($result) == 1) {
    $row = mysql_fetch_array($result);
    if ($row['user_id'] != $userinfo['user_id']) {
      die('No journal entry matching that ID found.');
    }
    mysql_query('UPDATE journal_entries SET entry_title="'.$entry_title.'", entry_text="'.$entry_text.'" WHERE entry_id="'.$entry_id.'"');
  } else {
    die("No journal entry matching that ID found.");
  }
}

# If we are reading a specific journal entry
if (isset($_GET['entry'])) {
  $entry_id = mysql_real_escape_string($_GET['entry']);
  $result = mysql_query('SELECT entry_title, entry_text FROM journal_entries ' .
                        'JOIN journals USING (journal_id) ' .
                        'JOIN characters USING (character_id) ' .
                        'WHERE character_id="'.$userinfo['current_character'].'" AND entry_id="'.$entry_id.'"');
  if (mysql_num_rows($result) == 0) {
    echo 'No matching journal entry found.';
  } else {
    $entry = mysql_fetch_array($result);
    ?>
    <div class="journal_entry_text">
      <div class="journal_entry_back"><a href='journal.php'>Back to journals</a></div>
      <form action='journal.php?entry=<?echo $entry_id?>' method='post'>
        <input type='hidden' name='entry_id' value='<?echo $entry_id?>'>
        <input type="text" name="entry_title" value="<?echo $entry['entry_title']?>"><br>
        <textarea name='entry_text'><?echo $entry['entry_text']?></textarea><br>
        <input type='submit' value='Save'>
      </form>
    </div>
    <?
  }

# If we are viewing a list of all journal entries
} else {

  $journal_result = mysql_query('SELECT journal_id, journal_name FROM journals JOIN characters USING (character_id) WHERE character_id="'.$userinfo['current_character'].'"');

  if (mysql_num_rows($journal_result) > 0) {

    echo '<div class="journal_entry_list">';

    while($journal = mysql_fetch_array($journal_result)) {

      echo '<div class="journal_name">'.$journal['journal_name'].'</div>';

      $entries_result = mysql_query('SELECT entry_id, entry_title FROM journal_entries WHERE journal_id="'.$journal['journal_id'].'" ORDER BY entry_id DESC');
      while($entry = mysql_fetch_array($entries_result)) {
        if (!$entry['entry_title']) {
          $entry['entry_title'] = '-';
        }
        echo '<a href="journal.php?entry='.$entry['entry_id'].'"><div class="journal_list_item">'.$entry['entry_title'].'</div></a>';
      }

      ?>
      <form action='journal.php' method="post">
        <input type="hidden" name="journal_id" value="<? echo $journal['journal_id'] ?>">
        <input type="submit" name="new_entry" value="New Entry">
      </form>
      <?
    }
  }

  ?>
  <br>
  <form action='journal.php' method="post">
    <input type="text" name="new_journal_name">
    <input type="submit" value="Create new journal">
  </form>
  <?
}
?>
</div>
