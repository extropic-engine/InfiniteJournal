<?
include 'header.php';
?>

<link rel="stylesheet" type="text/css" href="themes/<?echo $userinfo['game_name']?>/artifacts.css" />

<div class='page'>

<?

# If we are adding a new item
if (isset($_POST['new_item'])) {
  mysql_query('INSERT INTO items (character_id) VALUES ('.$userinfo['current_character'].')');
  $result = mysql_query('SELECT LAST_INSERT_ID() as item_id');
  $row = mysql_fetch_array($result);
  die('<meta http-equiv=refresh content="0;URL=artifacts.php?detail='.$row['item_id'].'">');
}

# If we are trying to update an item's details
if (isset($_POST['item_id'])) {
  $item_id = mysql_real_escape_string($_POST['item_id']);
  $item_name = mysql_real_escape_string($_POST['item_name']);
  $item_description = mysql_real_escape_string($_POST['item_description']);
  $result = mysql_query('SELECT user_id FROM items JOIN characters USING (character_id) JOIN users USING (user_id) WHERE item_id="'.$item_id.'"');
  if (mysql_num_rows($result) == 1) {
    $row = mysql_fetch_array($result);
    if ($row['user_id'] != $userinfo['user_id']) {
      die('No item matching that ID found.');
    }
    mysql_query('UPDATE items SET name="'.$item_name.'", description="'.$item_description.'" WHERE item_id="'.$item_id.'"');
  } else {
    die("No item matching that ID found.");
  }
}

# If we are viewing a specific item in detail
if (isset($_GET['detail'])) {
  $item_id = mysql_real_escape_string($_GET['detail']);
  $result = mysql_query('SELECT items.name AS name, description FROM items ' .
                        'JOIN characters USING (character_id) ' .
                        'WHERE character_id="'.$userinfo['current_character'].'" AND item_id="'.$item_id.'"');
  if (mysql_num_rows($result) == 0) {
    echo 'No matching item found.';
  } else {
    $item = mysql_fetch_array($result);
    ?>
    <div class="item_details">
      <div class="item_details_back"><a href='artifacts.php'>Back to all artifacts</a></div>
      <form action='artifacts.php?details=<?echo $item_id?>' method='post'>
        <input type='hidden' name='item_id' value='<?echo $item_id?>'>
        <input type="text" name="item_name" value="<?echo $item['name']?>">
        <textarea name='item_description'><?echo $item['description']?></textarea><br>
        <input type='submit' value='Save'>
      </form>
    </div>
    <?
  }

# If we are viewing a list of all items
} else {

  $item_result = mysql_query('SELECT item_id, items.name AS name FROM items JOIN characters USING (character_id) WHERE character_id="'.$userinfo['current_character'].'" ORDER BY item_id');

  if (mysql_num_rows($item_result) > 0) {

    echo '<div class="item_list">';
    echo '<br><div class="item_list_header">Artifacts</div>';

    while($item = mysql_fetch_array($item_result)) {
      if (!$item['name']) {
        $item['name'] = 'Unnamed';
      }
      echo '<a href="artifacts.php?detail='.$item['item_id'].'"><div class="item_list_item">'.$item['name'].'</div></a>';
    }
  }

  ?>
    <form action='artifacts.php' method="post">
      <input type="submit" name="new_item" value="New Artifact">
    </form>
  <?

 }
?>
</div>
