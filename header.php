<?
include 'login.php'
?>

<head>
<title>Infinite Journal</title>
<link rel="stylesheet" type="text/css" href="themes/<?echo $userinfo['game_name']?>/style.css" />
</head>

<div class='top-nav'>
  <a href='index.php'><span class='welcome'>Welcome, <? echo $userinfo['fullname']; ?>.</span></a>
  <form action='change-character.php' method='post'>
    <select name='character'>
      <?
      $characters = mysql_query('SELECT * FROM characters WHERE user_id="'.$userinfo['user_id'].'" AND game_id="'.$userinfo['current_game'].'"') or die('Query failed');
      while ($character = mysql_fetch_array($characters)) {
        if ($character['character_id'] == $userinfo['current_character']) {
          echo '<option selected value="'.$character['character_id'].'">'.$character['name'].'</option>';
        } else {
          echo '<option value="'.$character['character_id'].'">'.$character['name'].'</option>';
        }
      }
      ?>
    </select>
    <input type='hidden' name='redirect' value='<?echo $_SERVER['PHP_SELF']?>'>
    <input type='submit' value='Change character'>
  </form>
  <form action='change-game.php' method='post'>
    <select name='game'>
      <?
      $games = mysql_query('SELECT * FROM games JOIN owned_games USING (game_id) WHERE user_id='.$userinfo['user_id']) or die('Query failed');
      while ($game = mysql_fetch_array($games)) {
        if ($game['game_id'] == $userinfo['current_game']) {
          echo '<option selected value="'.$game['game_id'].'">'.$game['game_name'].'</option>';
        } else {
          echo '<option value="'.$game['game_id'].'">'.$game['game_name'].'</option>';
        }
      }
      ?>
    </select>
    <input type='hidden' name='redirect' value='<?echo $_SERVER['PHP_SELF']?>'>
    <input type='submit' value='Change game'>
  </form>
  <form action='logout.php'>
    <input type='submit' value='Logout'>
  </form>
</div>

<div class='left-nav'>
<ul>
  <li><a href='journal.php'>Journal</a></li>
  <li><a href='quests.php'>Quests</a></li>
  <li><a href='people.php'>People</a></li>
  <li><a href='maps.php'>Maps</a></li>
  <li><a href='artifacts.php'>Artifacts</a></li>
</ul>
</div>

<a href='options.php'>
  <div class="options-button">
  </div>
</a>
