<?
include 'header.php';
?>

<link rel="stylesheet" type="text/css" href="themes/<?echo $userinfo['game_name']?>/quests.css" />

<div class='page'>

<?

# If we are adding a new quest
if (isset($_POST['new_quest'])) {
  mysql_query('INSERT INTO quests (character_id) VALUES ('.$userinfo['current_character'].')');
  $result = mysql_query('SELECT LAST_INSERT_ID() as quest_id');
  $row = mysql_fetch_array($result);
  die('<meta http-equiv=refresh content="0;URL=quests.php?detail='.$row['quest_id'].'">');
}

# If we are trying to update a quest
if (isset($_POST['quest_id'])) {
  $quest_id = mysql_real_escape_string($_POST['quest_id']);
  $quest_name = mysql_real_escape_string($_POST['quest_name']);
  $quest_description = mysql_real_escape_string($_POST['quest_description']);
  $result = mysql_query('SELECT user_id FROM quests JOIN characters USING (character_id) JOIN users USING (user_id) WHERE quest_id="'.$quest_id.'"');
  if (mysql_num_rows($result) == 1) {
    $row = mysql_fetch_array($result);
    if ($row['user_id'] != $userinfo['user_id']) {
      die('No quest matching that ID found.');
    }
    mysql_query('UPDATE quests SET name="'.$quest_name.'", description="'.$quest_description.'" WHERE quest_id="'.$quest_id.'"');
  } else {
    die("No quest matching that ID found.");
  }
}

# If we are viewing a specific quest in detail
if (isset($_GET['detail'])) {
  $quest_id = mysql_real_escape_string($_GET['detail']);
  $result = mysql_query('SELECT quests.name AS name, description FROM quests ' .
                        'JOIN characters USING (character_id) ' .
                        'WHERE character_id="'.$userinfo['current_character'].'" AND quest_id="'.$quest_id.'"');
  if (mysql_num_rows($result) == 0) {
    echo 'No matching quest found.';
  } else {
    $quest = mysql_fetch_array($result);
    ?>
    <div class="quest_details">
      <div class="quest_details_back"><a href='quests.php'>Back to all quests</a></div>
      <form action='quests.php?details=<?echo $quest_id?>' method='post'>
        <input type='hidden' name='quest_id' value='<?echo $quest_id?>'>
        <input type="text" name="quest_name" value="<?echo $quest['name']?>">
        <textarea name='quest_description'><?echo $quest['description']?></textarea><br>
        <input type='submit' value='Save'>
      </form>
    </div>
    <?
  }

# If we are viewing a list of all quests
} else {

  $quest_result = mysql_query('SELECT quest_id, quests.name AS name FROM quests JOIN characters USING (character_id) WHERE character_id="'.$userinfo['current_character'].'" ORDER BY quest_id');

  if (mysql_num_rows($quest_result) > 0) {

    echo '<div class="quest_list">';
    echo '<br><div class="quest_list_header">Quests</div>';

    while($quest = mysql_fetch_array($quest_result)) {
      if (!$quest['name']) {
        $quest['name'] = 'Unnamed';
      }
      echo '<a href="quests.php?detail='.$quest['quest_id'].'"><div class="quest_list_item">'.$quest['name'].'</div></a>';
    }
  }

  ?>
    <form action='quests.php' method="post">
      <input type="submit" name="new_quest" value="New Quest">
    </form>
  <?

 }
?>
</div>
