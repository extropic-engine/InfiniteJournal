$(document).ready(function() {

  // Get handles to the canvas and temp canvas

  var container = document.getElementsByClassName('canvas_container')[0];
  var canvas0 = document.getElementById('map_canvas');
  canvas0.width = container.scrollWidth;
  canvas0.height = container.scrollHeight;
  var context0 = canvas0.getContext('2d');

  var canvas = document.createElement('canvas');
  canvas.id = 'temp_canvas';
  canvas.width = canvas0.width;
  canvas.height = canvas0.height;
  container.appendChild(canvas);
  context = canvas.getContext('2d');

  // Get the handle to the canvas that shows the brush width and color.
  var brush_canvas = document.getElementById('brush_canvas');
  var brush_context = brush_canvas.getContext('2d');

  // Load the previously saved version of the map

  context0.fillStyle = 'white';
  context0.fillRect(0,0,canvas0.width, canvas0.height);
  var img = new Image();
  img.src = "user_maps/test.png";
  img.onload = function() {
    context0.drawImage(img, 0, 0);
  };

  // Initialize the brush width/color
  context.strokeStyle = 'black';
  context.fillStyle = 'white';
  update_brush();

  var current_tool = new tool_pencil();

  // Set up callback functions for all the tool buttons

  // Color picker

  $('#white').click(function(e)   { context.strokeStyle = 'white'; update_brush(); });
  $('#yellow').click(function(e)  { context.strokeStyle = 'yellow'; update_brush(); });
  $('#fuchsia').click(function(e) { context.strokeStyle = 'fuchsia'; update_brush(); });
  $('#red').click(function(e)     { context.strokeStyle = 'red'; update_brush(); });
  $('#silver').click(function(e)  { context.strokeStyle = 'silver'; update_brush(); });
  $('#gray').click(function(e)    { context.strokeStyle = 'gray'; update_brush(); });
  $('#olive').click(function(e)   { context.strokeStyle = 'olive'; update_brush(); });
  $('#purple').click(function(e)  { context.strokeStyle = 'purple'; update_brush(); });
  $('#maroon').click(function(e)  { context.strokeStyle = 'maroon'; update_brush(); });
  $('#aqua').click(function(e)    { context.strokeStyle = 'aqua'; update_brush(); });
  $('#lime').click(function(e)    { context.strokeStyle = 'lime'; update_brush(); });
  $('#teal').click(function(e)    { context.strokeStyle = 'teal'; update_brush(); });
  $('#green').click(function(e)   { context.strokeStyle = 'green'; update_brush(); });
  $('#blue').click(function(e)    { context.strokeStyle = 'blue'; update_brush(); });
  $('#navy').click(function(e)    { context.strokeStyle = 'navy'; update_brush(); });
  $('#black').click(function(e)   { context.strokeStyle = 'black'; update_brush(); });

  $('#white').bind('contextmenu', function(e)   { e.preventDefault(); context.fillStyle = 'white'; update_brush(); return false; });
  $('#yellow').bind('contextmenu', function(e)  { e.preventDefault(); context.fillStyle = 'yellow'; update_brush(); return false; });
  $('#fuchsia').bind('contextmenu', function(e) { e.preventDefault(); context.fillStyle = 'fuchsia'; update_brush(); return false; });
  $('#red').bind('contextmenu', function(e)     { e.preventDefault(); context.fillStyle = 'red'; update_brush(); return false; });
  $('#silver').bind('contextmenu', function(e)  { e.preventDefault(); context.fillStyle = 'silver'; update_brush(); return false; });
  $('#gray').bind('contextmenu', function(e)    { e.preventDefault(); context.fillStyle = 'gray'; update_brush(); return false; });
  $('#olive').bind('contextmenu', function(e)   { e.preventDefault(); context.fillStyle = 'olive'; update_brush(); return false; });
  $('#purple').bind('contextmenu', function(e)  { e.preventDefault(); context.fillStyle = 'purple'; update_brush(); return false; });
  $('#maroon').bind('contextmenu', function(e)  { e.preventDefault(); context.fillStyle = 'maroon'; update_brush(); return false; });
  $('#aqua').bind('contextmenu', function(e)    { e.preventDefault(); context.fillStyle = 'aqua'; update_brush(); return false; });
  $('#lime').bind('contextmenu', function(e)    { e.preventDefault(); context.fillStyle = 'lime'; update_brush(); return false; });
  $('#teal').bind('contextmenu', function(e)    { e.preventDefault(); context.fillStyle = 'teal'; update_brush(); return false; });
  $('#green').bind('contextmenu', function(e)   { e.preventDefault(); context.fillStyle = 'green'; update_brush(); return false; });
  $('#blue').bind('contextmenu', function(e)    { e.preventDefault(); context.fillStyle = 'blue'; update_brush(); return false; });
  $('#navy').bind('contextmenu', function(e)    { e.preventDefault(); context.fillStyle = 'navy'; update_brush(); return false; });
  $('#black').bind('contextmenu', function(e)   { e.preventDefault(); context.fillStyle = 'black'; update_brush(); return false; });

  // Tool switcher

  $('#pencil').click(function(e) { current_tool = new tool_pencil(); });
  $('#rect').click(function(e) { current_tool = new tool_rectangle();});
  $('#line').click(function(e) { current_tool = new tool_line();});
  $('#clear').click(function(e) {
    if (confirm('Are you sure?')) {
      context0.fillStyle = context.fillStyle;
      context0.fillRect(0,0,canvas0.width, canvas0.height);
    }
  });

  $('#save').click(function(e) {
    var canvasData = canvas0.toDataURL("image/png");
    var ajax = new XMLHttpRequest();
    ajax.open("POST",'maps_save.php',false);
    ajax.setRequestHeader('Content-Type', 'application/upload');
    ajax.send(canvasData);
  });

  $('#brush_canvas').click(function(e) {
    e.preventDefault();
    context.lineWidth += 1.0;
    if (context.lineWidth > 10.0) {
      context.lineWidth = 1.0;
    }
    update_brush();
  });

  function update_brush() {
    brush_context.fillStyle = context.fillStyle;
    brush_context.fillRect(0,0,brush_canvas.width, brush_canvas.height);
    brush_context.fillStyle = context.strokeStyle;
    brush_context.beginPath();
    brush_context.arc(brush_canvas.width/2,brush_canvas.height/2,context.lineWidth,0,Math.PI*2,true);
    brush_context.closePath();
    brush_context.fill();
  }

  // Set up callback functions for clicking the canvas

  $('#temp_canvas').mousedown(function(e){
    e.preventDefault();
    var x = e.offsetX;
    var y = e.offsetY;
    current_tool.mousedown(x,y);
  });

  $('#temp_canvas').mousemove(function(e){
    e.preventDefault();
    var x = e.offsetX;
    var y = e.offsetY;
    current_tool.mousemove(x,y);
  });

  $('#temp_canvas').mouseup(function(e){
    var x = e.offsetX;
    var y = e.offsetY;
    current_tool.mouseup(x,y);
  });

  function canvas_write() {
    context0.drawImage(canvas, 0, 0);
    context.clearRect(0,0,canvas.width, canvas.height);
  }

  /************************
   * Tool Implementations *
   ************************/

  // Pencil tool

  function tool_pencil () {
    var tool = this;
    this.started = false;

    this.mousedown = function(x, y) {
      context.lineJoin = 'round';
      context.lineCap = 'round';
      context.beginPath();
      context.moveTo(x,y);
      tool.started = true;
    };

    this.mousemove = function(x,y) {
      if (tool.started) {
        context.lineTo(x,y);
        context.stroke();
      }
    };

    this.mouseup = function(x,y) {
      if (tool.started) {
        tool.mousemove(x,y);
        tool.started = false;
        canvas_write();
      }
    };
  }

  // Rectangle tool

  function tool_rectangle() {
    var tool = this;
    this.started = false;

    this.mousedown = function(x, y) {
      tool.started = true;
      tool.x0 = x;
      tool.y0 = y;
    };

    this.mousemove = function(x,y) {
      if (!tool.started) {
        return;
      }

      var r_x = Math.min(x,  tool.x0),
          r_y = Math.min(y,  tool.y0),
          r_w = Math.abs(x - tool.x0),
          r_h = Math.abs(y - tool.y0);

      context.clearRect(0, 0, canvas.width, canvas.height);

      if (!r_w || !r_h) {
        return;
      }

      context.strokeRect(r_x, r_y, r_w, r_h);
    };

    this.mouseup = function(x,y) {
      if (tool.started) {
        tool.mousemove(x,y);
        tool.started = false;
        canvas_write();
      }
    };
  }

  // Line tool

  function tool_line() {
    var tool = this;
    this.started = false;

    this.mousedown = function(x, y) {
      tool.started = true;
      tool.x0 = x;
      tool.y0 = y;
    };

    this.mousemove = function(x,y) {
      if (!tool.started) {
        return;
      }

      context.clearRect(0, 0, canvas.width, canvas.height);

      context.beginPath();
      context.moveTo(tool.x0, tool.y0);
      context.lineTo(x, y);
      context.stroke();
      context.closePath();
    };

    this.mouseup = function(x,y) {
      if (tool.started) {
        tool.mousemove(x,y);
        tool.started = false;
        canvas_write();
      }
    };
  }

});


